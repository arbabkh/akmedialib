
Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.name         = "AKMediaLib"
  spec.version      = "0.0.1"
  spec.summary      = "AKMediaLib ease many things to developer."

  spec.description  = <<-DESC
						AKMediaLib ease many things to developer likewise pick user's camera roll, documents from drives. User can edit media from this library.
                   DESC

  spec.homepage     = "http://logiray.com"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

   spec.license      = { :type => "MIT", :file => "LICENSE" }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.author             = { "Arbab Ahmed Khan" => "arbabkh@gmail.com" }

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
   
   spec.platform     = :ios, "12.0"

   spec.ios.deployment_target = "12.0"


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.source       = { :git => "https://arbabkh@bitbucket.org/arbabkh/akmedialib_ios.git", :tag => "#{spec.version}" }


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.source_files = "AKMediaLib/Source/**/*.{swift}"
  # spec.exclude_files = "Classes/Exclude"

  # spec.public_header_files = "Classes/**/*.h"

  # ――― Dependencies ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
		
		spec.dependency "SVGKit"
	  spec.dependency "SDWebImage"

  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  # spec.resource  = "icon.png"
  #	spec.resources = "AKMediaLib/Source/**/*.{storyboard,xib}"
	spec.resource_bundles = { 'AKMediaLib' => ['AKMediaLib/Source/**/*.{storyboard,xib}'] }
	spec.resources = 'AKMediaLib/Source/AKMedia.bundle'

#	spec.resource_bundles = {
#		'AKMediaLib' => ['AKMediaLib/Source/AKMediaView/**/*.{storyboard,xib}']
#	}
  # spec.preserve_paths = "FilesToSave", "MoreFilesToSave"


end
