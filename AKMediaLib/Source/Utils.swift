//
//  Utils.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 29/08/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import Foundation
import UIKit

class Utils: NSObject {
	class public func getAspectRatioAccordingToiPhones(cellImageWidth: CGFloat,
										  mediaSize: CGSize) -> CGFloat {
        let widthOffset = mediaSize.width - cellImageWidth
        let widthOffsetPercentage = (widthOffset*100)/mediaSize.width
        let heightOffset = (widthOffsetPercentage * mediaSize.height)/100
        let effectiveHeight = mediaSize.height - heightOffset
        return (effectiveHeight)
    }
}
