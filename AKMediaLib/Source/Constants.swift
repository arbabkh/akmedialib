//
//  Constants.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 29/06/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import Foundation
import UIKit

class Constants {

	static var frameworkBundle:Bundle? {
		let bundleId = "com.logiray.AKMediaLib"
		return Bundle(identifier: bundleId)
	}
	
	static var mainScreenSize : CGSize {
		return UIScreen.main.bounds.size
	}
}
