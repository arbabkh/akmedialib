//
//  AKMediaListTC.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 29/08/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import UIKit

class AKMediaListTC: UITableViewCell {

	@IBOutlet weak var containerImageView: UIView!
	var viewContentMode = UIView.ContentMode.scaleAspectFill

	var mMedia : CMAppMedia?

	lazy var mediaImageView : AKMediaImageView = {
		var mediaImageView = AKBundle.bundle().loadNibNamed("AKMediaImageView", owner: nil, options: nil)?.first as? AKMediaImageView
		mediaImageView?.frame = CGRect(x: 0, y: 0, width: containerImageView.frame.size.width, height: containerImageView.frame.size.height)
		mediaImageView?.viewContentMode = viewContentMode
		return mediaImageView!
	}()

    override func awakeFromNib() {
        super.awakeFromNib()
		containerImageView.addSubview(mediaImageView)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	open func updateCellWithData(inMedia : CMAppMedia?) {
		mMedia = inMedia
		mediaImageView.updateCellWithData(inMedia: mMedia)
		setNeedsLayout()
	}
		
	open override func layoutSubviews() {
		super.layoutSubviews()
		
		mediaImageView.frame = CGRect(x: 0,
									  y: 0,
									  width: containerImageView.frame.size.width,
									  height: containerImageView.frame.size.height)
	}

}
