//
//  AKMediaView.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 27/06/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import UIKit
//import TLPhotoPicker

open class AKMediaView: UIView {
	
	open var isRoundedCorners = false
	open var isPagingEnabled = false
	open var isHideMuteButton = false
	open var isEditingMode = false
	open var isShowCollargeView = false
	open var isFullView = false
	open var horizontalSpacing: CGFloat = 0.0
	open var widthIcon:CGFloat = 100
	open var heightIcon:CGFloat = 100
	open var placeHolderImage: UIImage?
	open var autoPlay = false
	open var hideEditBtn = true //temp

	var mediaCount:Int = 0

	@IBOutlet weak var collectionView: UICollectionView!
	
	var datasource : [CMAppMedia]?
//	var datasource : [TLPHAsset]?

	open var didSelectAt: ((IndexPath)->Void)?
	open var didActionCallBack: ((Any, String)->Void)?
	open var didAppMediaScrollViewDidEndDecelerating: ((UIScrollView)->Void)?
	open var didAppMediaScrollViewDidScroll: ((UIScrollView)->Void)?

	open override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
		self.collectionView.register(UINib(nibName: "AKMediaImageCollectionCell", bundle: AKBundle.bundle()), forCellWithReuseIdentifier: "AKMediaImageCollectionCell")
		self.collectionView.register(UINib(nibName: "AKMediaVideoCollectionCell", bundle: AKBundle.bundle()), forCellWithReuseIdentifier: "AKMediaVideoCollectionCell")
		self.collectionView.register(UINib(nibName: "AKMediaDocCollectionCell", bundle: AKBundle.bundle()), forCellWithReuseIdentifier: "AKMediaDocCollectionCell")

		
		
	}
	open func updateCellWithData(inList:[CMAppMedia]?) {
		self.collectionView.isPagingEnabled = isPagingEnabled
		datasource = inList
		if datasource?.count ?? 0 > 0 {
			mediaCount = datasource!.count
		}
		self.collectionView.collectionViewLayout.invalidateLayout()
		self.collectionView.reloadData()
		setNeedsLayout()

	}
	
//	open func updateCellWithTLPHAssetData(inList:[TLPHAsset]?) {
//		self.collectionView.isPagingEnabled = isPagingEnabled
//		datasource = inList
//		if datasource?.count ?? 0 > 0 {
//			mediaCount = datasource!.count
//		}
//		self.collectionView.collectionViewLayout.invalidateLayout()
//		self.collectionView.reloadData()
//		setNeedsLayout()
//
//	}

	
	open func playVisibleCenteredVideo() {
		if datasource?.count ?? 0 > 0 {
			if datasource?.count == 1 {
				let visibleRect = CGRect(x: self.collectionView.contentOffset.x, y: self.collectionView.contentOffset.y, width: self.collectionView.frame.size.width, height: self.collectionView.frame.size.height)
				let visiblePoint  = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
				let visibleIndexPath = self.collectionView.indexPathForItem(at: visiblePoint)
				handle(video: true, atIndex: visibleIndexPath?.item ?? 0)
			}
		}
	}
	
	func handle(video inIsPlay: Bool,atIndex inIndex: Int) {
		let media = datasource![inIndex]
		if inIsPlay {
			AKVideoMgr.shared.playVideo(idMedia: media)
		} else {
			AKVideoMgr.shared.pauseVideo(idMedia: media)
		}
	}
}
extension AKMediaView: UICollectionViewDelegate {
	open func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		didSelectAt?(indexPath)
	}
}

extension AKMediaView: UIScrollViewDelegate {
	open func scrollViewDidScroll(_ scrollView: UIScrollView) {
		didAppMediaScrollViewDidScroll?(scrollView)
	}
	open func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
		AKVideoMgr.shared.pauseAllVideos()
	}
	open func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
		if !decelerate {
			playVisibleCenteredVideo()
		}
	}
	open func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		AKVideoMgr.shared.pauseAllVideos()
		didAppMediaScrollViewDidEndDecelerating?(scrollView)
	}
}

extension AKMediaView: UICollectionViewDataSource {
	
	open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return datasource?.count ?? 0
	}

	open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let appMedia = datasource?[indexPath.item]
		if appMedia?.appMediaType == enAppMediaType.eMediaTypeVideo ||
			appMedia?.appMediaType == enAppMediaType.eMediaTypeDefaultLocalVideo {
			return getMediaVideoCollectionCell(collectionView: collectionView, indexPath: indexPath)
		} else if appMedia?.appMediaType == enAppMediaType.eMediaTypeDocFile {
			return getMediaDocCollectionCell(collectionView: collectionView, indexPath: indexPath)
		} else {
			return getMediaImageCollectionCell(collectionView: collectionView, indexPath: indexPath)
		}
	}
	
	func getMediaImageCollectionCell(collectionView: UICollectionView, indexPath : IndexPath) -> AKMediaImageCollectionCell {
		let appMedia = datasource?[indexPath.item]
		let aCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AKMediaImageCollectionCell", for: indexPath) as! AKMediaImageCollectionCell
		aCell.isRoundedCorners = isRoundedCorners
		aCell.indexPath = indexPath
		aCell.widthIcon = widthIcon
		aCell.heightIcon = heightIcon
		aCell.placeHolderImage = placeHolderImage
		aCell.isEditingMode = isEditingMode
		aCell.hideEditBtn = hideEditBtn
		aCell.updateCellWithData(inMedia: appMedia)
		aCell.didActionCallBack = {[weak self] (obj, actionId) in
			self?.didActionCallBack?(obj, actionId)
		}
		return aCell
	}
	
	func getMediaDocCollectionCell(collectionView: UICollectionView, indexPath : IndexPath) -> AKMediaDocCollectionCell {
		let appMedia = datasource?[indexPath.item]
		let aCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AKMediaDocCollectionCell", for: indexPath) as! AKMediaDocCollectionCell
		aCell.isRoundedCorners = isRoundedCorners
		aCell.widthIcon = widthIcon
		aCell.heightIcon = heightIcon
		aCell.placeHolderImage = placeHolderImage
		aCell.isEditingMode = isEditingMode
		aCell.hideEditBtn = hideEditBtn
		aCell.updateCellWithData(inMedia: appMedia)
		aCell.didActionCallBack = {[weak self] (obj, actionId) in
			self?.didActionCallBack?(obj, actionId)
		}
		return aCell
	}
	
	func getMediaVideoCollectionCell(collectionView: UICollectionView, indexPath : IndexPath) -> AKMediaVideoCollectionCell {
		let appMedia = datasource?[indexPath.item]
		let aCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AKMediaVideoCollectionCell", for: indexPath) as! AKMediaVideoCollectionCell
		aCell.isRoundedCorners = isRoundedCorners
		aCell.isHideMuteButton = isHideMuteButton
		aCell.autoPlay = autoPlay
		aCell.isShowThumbnail = datasource?.count ?? 0 > 1 ? true : false
		aCell.widthIcon = widthIcon
		aCell.heightIcon = heightIcon
		aCell.placeHolderImage = placeHolderImage
		aCell.isEditingMode = isEditingMode
		aCell.hideEditBtn = hideEditBtn
		aCell.updateCellWithData(inMedia: appMedia)
		aCell.didActionCallBack = {[weak self] (obj, actionId) in
			self?.didActionCallBack?(obj, actionId)
		}
		return aCell
	}
	
	
	
}

extension AKMediaView: UICollectionViewDelegateFlowLayout {
	
	open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		let appMedia = datasource?[indexPath.item]
		if isShowCollargeView {
			var width:CGFloat = (collectionView.frame.size.width - 3)/2
			var height:CGFloat = collectionView.frame.size.height
			if mediaCount == 1 {
				width = collectionView.frame.size.width
			}
			if mediaCount == 3 {
				if indexPath.item != 0 {
					width = (collectionView.frame.size.width - 3)/2.3
					height = (collectionView.frame.size.height - 3)/2
				} else {
					width = (collectionView.frame.size.width - 3)/1.7
				}
			}
			if mediaCount == 4 {
				if indexPath.item != 0 {
					width = (collectionView.frame.size.width - 3)/2.3
					height = (collectionView.frame.size.height - 6)/3
				} else {
					width = (collectionView.frame.size.width - 3)/1.7
				}
			}
			if mediaCount == 5 {
				width = (collectionView.frame.size.width - 3)/2
				if indexPath.item != 0 && indexPath.item != 1 {
					height = (collectionView.frame.size.height - 6)/3
				} else {
					height = (collectionView.frame.size.height - 3)/2
				}
			}
			appMedia?.width = width
			appMedia?.height = height
			return CGSize(width: width, height: height)
		} else {
			if isFullView {
				let width = collectionView.frame.size.width/1.5
				let height = collectionView.frame.size.height
				appMedia?.width = width
				appMedia?.height = height
				return CGSize(width: width, height: height)
			} else {
				let width = collectionView.frame.size.width
				let height = collectionView.frame.size.height
				appMedia?.width = width
				appMedia?.height = height
				return CGSize(width: width, height: height)
			}
		}
	}
	open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
		if isShowCollargeView {
			return 3
		} else {
			return horizontalSpacing
		}
	}

	open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
		if isShowCollargeView {
			return 3
		} else {
			return horizontalSpacing
		}
	}
	open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
		return CGSize.zero
	}

	open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
		return CGSize.zero
	}
	open func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		return .zero
	}
}
