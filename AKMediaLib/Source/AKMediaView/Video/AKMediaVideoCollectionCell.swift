//
//  AKMediaVideoCollectionCell.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 27/06/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import UIKit

open class AKMediaVideoCollectionCell: UICollectionViewCell {

	@IBOutlet weak var containerVideoView: UIView!
	open var isHideMuteButton = false
	open var isRoundedCorners = false
	open var isShowThumbnail = false
	open var autoPlay = false
	open var isEditingMode = false
	open var hideEditBtn = true //temp

	open var widthIcon:CGFloat = 100
	open var heightIcon:CGFloat = 100
	open var placeHolderImage: UIImage?

	open var mMedia : CMAppMedia?

	open var didActionCallBack: ((Any, String)->Void)?

	open lazy var mediaVideoView : AKMediaVideoView = {
		var mediaVideoView = AKBundle.bundle().loadNibNamed("AKMediaVideoView", owner: self, options: nil)?.first as? AKMediaVideoView
		mediaVideoView?.frame = CGRect(x: 0, y: 0, width: containerVideoView.frame.size.width, height: containerVideoView.frame.size.height)
		return mediaVideoView!
	}()

	open override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		containerVideoView.addSubview(mediaVideoView)
    }

	open func updateCellWithData(inMedia : CMAppMedia?) {
		mMedia = inMedia
		mediaVideoView.isRoundedCorners = isRoundedCorners
		mediaVideoView.isHideMuteButton = isHideMuteButton
		mediaVideoView.isShowThumbnail = isShowThumbnail
		mediaVideoView.autoPlay = autoPlay
		mediaVideoView.widthIcon = widthIcon
		mediaVideoView.heightIcon = heightIcon
		mediaVideoView.placeHolderImage = placeHolderImage
		mediaVideoView.isEditingMode = isEditingMode
		mediaVideoView.hideEditBtn = hideEditBtn
		mediaVideoView.updateCellWithData(inMedia: mMedia)
		mediaVideoView.didActionCallBack = {[weak self] (obj, actionId) in
			self?.didActionCallBack?(obj, actionId)
		}
		setNeedsLayout()
	}
		
	open override func layoutSubviews() {
		super.layoutSubviews()
		
		mediaVideoView.frame = CGRect(x: 0,
									  y: 0,
									  width: containerVideoView.frame.size.width,
									  height: containerVideoView.frame.size.height)
	}

}
