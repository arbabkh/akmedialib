//
//  AKMediaVideoView.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 27/06/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import UIKit

open class AKMediaVideoView: UIView {
	@IBOutlet weak var containerViewImgPic: UIView!
	@IBOutlet weak var btnMute: UIButton!
	@IBOutlet open weak var btnRemove: UIButton!
	@IBOutlet open weak var btnRemoveView: AKCustomView!
	@IBOutlet open weak var btnEdit: UIButton!
	@IBOutlet open weak var btnEditView: AKCustomView!

	@IBOutlet open weak var btnPlay: UIButton!
	@IBOutlet weak var widthContraintBtnPlay: NSLayoutConstraint!
	@IBOutlet weak var heightContraintBtnPlay: NSLayoutConstraint!

	open var isHideMuteButton = false

	open var mMedia : CMAppMedia?
	open var isRoundedCorners = false
	open var isShowThumbnail = false
	open var autoPlay = false
	open var isEditingMode = false
	open var hideEditBtn = true //temp

	open var widthIcon:CGFloat = 100
	open var heightIcon:CGFloat = 100
	open var placeHolderImage: UIImage?

	open var themeColor: UIColor =  UIColor(red: 254/255, green: 76/255, blue: 76/255, alpha: 1.0)

	open var didActionCallBack: ((Any, String)->Void)?

	open lazy var akLayerPlayerView: AKPlayerView = {
		var aPlayerView = AKBundle.bundle().loadNibNamed("AKPlayerView", owner: self, options: nil)?.first as? AKPlayerView
		return aPlayerView!
	}()
	
	open override func awakeFromNib() {
		super.awakeFromNib()
		containerViewImgPic.insertSubview(akLayerPlayerView, at: 0)

		let removeIcon = AKBundle.podBundleImage(named: "crossIconWhite")
		btnRemove.setImage(removeIcon, for: .normal)
		btnRemove.tintColor = UIColor.white
	}
	
	@IBAction func clickedOnBtnMute(_ sender: Any) {
		didActionCallBack?(mMedia!, "muteMedia")
	}
	@IBAction func clickedOnBtnRemove(_ sender: Any) {
		didActionCallBack?(mMedia!, "deleteMedia")
	}
	@IBAction func clickedOnBtnEdit(_ sender: Any) {
		didActionCallBack?(mMedia!, "editMedia")
	}
    open func updateCellWithData(inMedia : CMAppMedia?) {
		mMedia = inMedia
		guard inMedia != nil else { return }
		if mMedia!.isMuted {
			let muteIcon = AKBundle.podBundleImage(named: "playerMuteIcon")
			btnMute.setImage(muteIcon, for: .normal)
		} else {
			let muteIcon = AKBundle.podBundleImage(named: "playerUnmuteIcon")
			btnMute.setImage(muteIcon, for: .normal)
		}
		playVideo(inMedia: inMedia!)
		setNeedsLayout()
	}
	
	func playVideo(inMedia : CMAppMedia?) {
		guard inMedia != nil else { return }
		AKVideoMgr.shared.addVideoInListIfNeeded(idMedia: inMedia!)
		
		let olderPlayer  = akLayerPlayerView.layer.sublayers?.first
		if olderPlayer is AKVideoPlayer {
			let aPlayer = olderPlayer
			aPlayer?.removeFromSuperlayer()
		}
		akLayerPlayerView.isHidden = false
		btnMute.isHidden = isHideMuteButton

		akLayerPlayerView.updateUIContentWithData(inMedia: inMedia!)
	}
	
	open override func layoutSubviews() {
		super.layoutSubviews()
		if isRoundedCorners {
			containerViewImgPic.layer.cornerRadius = 8
			containerViewImgPic.layer.borderColor = UIColor.lightGray.cgColor
			containerViewImgPic.layer.borderWidth = 0.5
		} else {
			containerViewImgPic.layer.cornerRadius = 0
			containerViewImgPic.layer.borderColor = UIColor.clear.cgColor
			containerViewImgPic.layer.borderWidth = 0.0
		}
		btnRemoveView.isHidden = !isEditingMode
		btnEditView.isHidden = hideEditBtn

		akLayerPlayerView.frame = CGRect(x: 0, y: 0, width: mMedia!.mediaSize.width, height: mMedia!.mediaSize.height)
		widthContraintBtnPlay.constant =  widthIcon
		heightContraintBtnPlay.constant = heightIcon

	}
}
