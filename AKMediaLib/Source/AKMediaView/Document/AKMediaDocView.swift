//
//  AKMediaDocView.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 27/06/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import UIKit

open class AKMediaDocView: UIView {
	
	@IBOutlet weak var containerViewImgPic: UIView!
	@IBOutlet open weak var btnRemove: UIButton!
	@IBOutlet open weak var btnRemoveView: AKCustomView!
	@IBOutlet open weak var btnEdit: UIButton!
	@IBOutlet open weak var btnEditView: AKCustomView!

	@IBOutlet open weak var placeHolderPic: UIButton!
	@IBOutlet weak var widthContraintBtnLogo: NSLayoutConstraint!
	@IBOutlet weak var heightContraintBtnLogo: NSLayoutConstraint!

	open var mMedia : CMAppMedia?
	open var isRoundedCorners = false
	open var isEditingMode = false
	open var hideEditBtn = true //temp

	open var widthIcon:CGFloat = 100
	open var heightIcon:CGFloat = 100
	open var placeHolderImage: UIImage?

	open var themeColor: UIColor =  UIColor(red: 254/255, green: 76/255, blue: 76/255, alpha: 1.0)
	

	open var didActionCallBack: ((Any, String)->Void)?

	open override func awakeFromNib() {
		super.awakeFromNib()
		let removeIcon = AKBundle.podBundleImage(named: "crossIconWhite")
		btnRemove.setImage(removeIcon, for: .normal)
		btnRemove.tintColor = UIColor.white
	}
    open func updateCellWithData(inMedia : CMAppMedia?) {
		mMedia = inMedia
		downloadDoc()
		setNeedsLayout()
	}
	
	@IBAction func clickedOnBtnRemove(_ sender: Any) {
		didActionCallBack?(mMedia!, "deleteMedia")
	}
	@IBAction func clickedOnBtnEdit(_ sender: Any) {
		didActionCallBack?(mMedia!, "editMedia")
	}
	func downloadDoc() {
		
	}
	
	open override func layoutSubviews() {
		super.layoutSubviews()
		if isRoundedCorners {
			containerViewImgPic.layer.cornerRadius = 8
			containerViewImgPic.layer.borderColor = UIColor.lightGray.cgColor
			containerViewImgPic.layer.borderWidth = 0.5
		} else {
			containerViewImgPic.layer.cornerRadius = 0
			containerViewImgPic.layer.borderColor = UIColor.clear.cgColor
			containerViewImgPic.layer.borderWidth = 0.0
		}
		btnRemoveView.isHidden = !isEditingMode
		btnEditView.isHidden = hideEditBtn

		widthContraintBtnLogo.constant =  widthIcon
		heightContraintBtnLogo.constant = heightIcon

	}

}
