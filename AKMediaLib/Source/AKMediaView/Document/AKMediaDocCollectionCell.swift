//
//  AKMediaDocCollectionCell.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 27/06/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import UIKit

open class AKMediaDocCollectionCell: UICollectionViewCell {

   	@IBOutlet weak var containerDocView: UIView!
	open var indexPath: IndexPath!

	open var isRoundedCorners = false
	open var isEditingMode = false
	open var hideEditBtn = true //temp

	open var mMedia : CMAppMedia?
	open var didActionCallBack: ((Any, String)->Void)?

	open var widthIcon:CGFloat = 100
	open var heightIcon:CGFloat = 100
	open var placeHolderImage: UIImage?

	open lazy var mediaDocView : AKMediaDocView = {
		var akMediaDocView = AKBundle.bundle().loadNibNamed("AKMediaDocView", owner: self, options: nil)?.first as? AKMediaDocView
		akMediaDocView?.frame = CGRect(x: 0, y: 0, width: containerDocView.frame.size.width, height: containerDocView.frame.size.height)
		return akMediaDocView!
	}()

	open override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		containerDocView.addSubview(mediaDocView)
    }

	open func updateCellWithData(inMedia : CMAppMedia?) {
		mMedia = inMedia
		mediaDocView.isRoundedCorners = isRoundedCorners
		mediaDocView.widthIcon = widthIcon
		mediaDocView.heightIcon = heightIcon
		mediaDocView.placeHolderImage = placeHolderImage
		mediaDocView.isEditingMode = isEditingMode
		mediaDocView.hideEditBtn = hideEditBtn
		mediaDocView.updateCellWithData(inMedia: mMedia)
		mediaDocView.didActionCallBack = {[weak self] (obj, actionId) in
			self?.didActionCallBack?(obj, actionId)
		}
		setNeedsLayout()
	}
		
	open override func layoutSubviews() {
		super.layoutSubviews()
		
		mediaDocView.frame = CGRect(x: 0,
									y: 0,
									width: containerDocView.frame.size.width,
									height: containerDocView.frame.size.height)
	}

}
