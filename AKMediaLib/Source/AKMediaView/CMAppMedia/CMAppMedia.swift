//
//  CMAppMedia.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 27/06/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import PhotosUI
import MobileCoreServices

public enum enAppMediaType : Int {
	
	case eMediaTypeNone = 0
	case eMediaTypeImage
	case eMediaTypeVideo
	case eMediaTypeDefaultLocalImage
	case eMediaTypeDefaultLocalVideo
	case eMediaTypeDocFile

}

public enum enAppMediaServiceState : Int {
	
	case eAppMediaServiceStateNone = 0
	case eAppMediaServiceStateStart
	case eAppMediaServiceStateInProgress
	case eAppMediaServiceStatePause
	case eAppMediaServiceStateDone
	case eAppMediaServiceStateError

}

open class CMAppMedia: NSObject, Codable, NSCoding {

	open var mediaId: String?
	open var imagePath: String?
	open var videoPath: String?
	open var thumbnName: String?
	open var mediaType: String?
	open var mediaName: String?
	open var mediaCaption: String?

	open var mediaLocalUrl: URL?
	open var strMediaLocalUrl: String?

	open var mediaExt: String?

	open var originalImage: UIImage?
	open var croppedImage: UIImage?

	open var mediaData: Data?
	open var mimetype: String?
	open var progressing: Double?
	open var isMediaSelected = false
	open var isAutoPlay = true
	open var isMuted = true

	open var appMediaServiceState: enAppMediaServiceState?

	open var width: CGFloat = 0
	open var height: CGFloat = 0
	
	var state = CloudDownloadState.ready
	public var phAsset: PHAsset? = nil
	//Bool to check if CMAppMedia returned is created using camera.
	public var isSelectedFromCamera = false
	public var selectedOrder: Int = 0
	
	enum CloudDownloadState {
		case ready, progress, complete, failed
	}
	
	public enum AssetType {
		case photo, video, livePhoto
	}
	
	public enum ImageExtType: String {
		case png, jpg, gif, heic
	}
	
	
//	var mediaHeight: CGFloat = 0.0
	
	var videoGraviity = AVLayerVideoGravity.resizeAspectFill

	enum CodingKeys: String, CodingKey {
		case mediaId
		case thumbnName = "videoThumb"
		case mediaType
		case mediaName
//		case mediaPath
		case mediaCaption
		case width = "mediaWidth"
		case height = "mediaHeight"
	}
	
	open var strMediaFullPath: String {
		guard mediaName != nil else {
			return ""
		}
		if appMediaType.rawValue == enAppMediaType.eMediaTypeVideo.rawValue {
			return !mediaName!.isEmpty ? (videoPath ?? "") + mediaName! : ""
		} else {
			return !mediaName!.isEmpty ? (imagePath ?? "") + mediaName! : ""
		}
	}
	
//	open var mediaFullPathUrl: URL {
//		return URL(string: strMediaFullPath!)!
//	}
	
	open var strThumbnFullPath: String? {
		guard thumbnName != nil else {
			return ""
		}
		return !thumbnName!.isEmpty ? (imagePath ?? "") + thumbnName! : ""
	}
	
//	open var thumbnFullPathUrl: URL? {
//		return URL(string: strThumbnFullPath!)
//	}
	
	open var mediaSize : CGSize {
		return CGSize(width: width, height: height)
	}
	open var mediaHeight : CGFloat {
		let width = Constants.mainScreenSize.width
		let mediaHeight = Utils.getAspectRatioAccordingToiPhones(cellImageWidth: width,
												  mediaSize: mediaSize)

		return mediaHeight
	}
	
		
	open var appMediaType : enAppMediaType {
		switch mediaType {
		case "image":
			return enAppMediaType.eMediaTypeImage
		case "video":
			return enAppMediaType.eMediaTypeVideo
		case "localImage":
			return enAppMediaType.eMediaTypeDefaultLocalImage
		case "localVideo":
			return enAppMediaType.eMediaTypeDefaultLocalVideo
		case "document":
			return enAppMediaType.eMediaTypeDocFile
		default:
			return enAppMediaType.eMediaTypeNone
		}
	}
	
	public override init() {
		super.init()

    }
	open func parseObject(inResponseObj:[String: Any]) {
		if let id = inResponseObj["mediaId"] as? String {
			mediaId = id
		}
		if let thumbnail = inResponseObj["videoThumb"] as? String {
			thumbnName = thumbnail
		}
		if let mediaT = inResponseObj["mediaType"] as? String {
			mediaType = mediaT
		}
		if let mediaN = inResponseObj["mediaName"] as? String {
			mediaName = mediaN
		}
//		if let mediaP = inResponseObj["mediaPath"] as? String {
//			mediaPath = mediaP
//		}
		if let mediaC = inResponseObj["mediaCaption"] as? String {
			mediaCaption = mediaC
		}
		if let wdth = inResponseObj["mediaWidth"] as? CGFloat {
			width = wdth
		}
		if let heith = inResponseObj["mediaHeight"] as? CGFloat {
			height = heith
		}
		

	}
	
	open func assignAppMediaCopy(inAppMedia: CMAppMedia) {
		mediaId = inAppMedia.mediaId
		thumbnName = inAppMedia.thumbnName
		mediaType = inAppMedia.mediaType
		mediaName = inAppMedia.mediaName
		mediaCaption = inAppMedia.mediaCaption
		imagePath = inAppMedia.imagePath
		videoPath = inAppMedia.videoPath
		width = inAppMedia.width
		height = inAppMedia.height
		phAsset = inAppMedia.phAsset

	}
	
	required public init(coder aDecoder: NSCoder) {
		mediaId = aDecoder.decodeObject(forKey: "mediaId") as? String
		thumbnName = aDecoder.decodeObject(forKey: "thumbnName") as? String
		mediaType = aDecoder.decodeObject(forKey: "mediaType") as? String
		mediaName = aDecoder.decodeObject(forKey: "mediaName") as? String
		mediaCaption = aDecoder.decodeObject(forKey: "mediaCaption") as? String
		imagePath = aDecoder.decodeObject(forKey: "imagePath") as? String
		videoPath = aDecoder.decodeObject(forKey: "videoPath") as? String
		let w:Float = aDecoder.decodeFloat(forKey: "width")
		width = w.f
		let h:Float = aDecoder.decodeFloat(forKey: "height")
		height = h.f
	}

	open func encode(with aCoder: NSCoder) {
		
		aCoder.encode(mediaId, forKey: "mediaId")
		aCoder.encode(thumbnName, forKey: "thumbnName")
		aCoder.encode(mediaType, forKey: "mediaType")
		aCoder.encode(mediaName, forKey: "mediaName")
		aCoder.encode(mediaCaption, forKey: "mediaCaption")
		aCoder.encode(imagePath, forKey: "imagePath")
		aCoder.encode(videoPath, forKey: "videoPath")
		aCoder.encode(width.swf, forKey: "width")
		aCoder.encode(height.swf, forKey: "height")

	}

	public var type: AssetType {
		get {
			guard let phAsset = self.phAsset else { return .photo }
			if phAsset.mediaSubtypes.contains(.photoLive) {
				return .livePhoto
			}else if phAsset.mediaType == .video {
				return .video
			}else {
				return .photo
			}
		}
	}
	
	public var fullResolutionImage: UIImage? {
		get {
			guard let phAsset = self.phAsset else { return nil }
			return AKPhotoLibrary.fullResolutionImageData(asset: phAsset)
		}
	}
	
	public func extType() -> ImageExtType {
		var ext = ImageExtType.png
		if let fileName = self.originalFileName, let extention = URL(string: fileName)?.pathExtension.lowercased() {
			ext = ImageExtType(rawValue: extention) ?? .png
		}
		return ext
	}
	public func replaceExtAsPNGIfNeeded() -> String {
		var name = self.originalFileName ?? "unknown.png"
		if let fileName = self.originalFileName, let extention = URL(string: fileName)?.pathExtension.lowercased() {
			let lowerC = fileName.lowercased()
			if extention == "png" {
				name = fileName
			} else {
				name = lowerC.replacingOccurrences(of: extention, with: "png")
			}
		}
		return name
	}
	@discardableResult
	public func cloudImageDownload(progressBlock: @escaping (Double) -> Void, completionBlock:@escaping (UIImage?)-> Void ) -> PHImageRequestID? {
		guard let phAsset = self.phAsset else { return nil }
		return AKPhotoLibrary.cloudImageDownload(asset: phAsset, progressBlock: progressBlock, completionBlock: completionBlock)
	}
	
	public var originalFileName: String? {
		get {
			guard let phAsset = self.phAsset,let resource = PHAssetResource.assetResources(for: phAsset).first else { return nil }
			return resource.originalFilename
		}
	}
	
	public func photoSize(options: PHImageRequestOptions? = nil ,completion: @escaping ((Int)->Void), livePhotoVideoSize: Bool = false) {
		guard let phAsset = self.phAsset, self.type == .photo || self.type == .livePhoto else { completion(-1); return }
		var resource: PHAssetResource? = nil
		if phAsset.mediaSubtypes.contains(.photoLive) == true, livePhotoVideoSize {
			resource = PHAssetResource.assetResources(for: phAsset).filter { $0.type == .pairedVideo }.first
		}else {
			resource = PHAssetResource.assetResources(for: phAsset).filter { $0.type == .photo }.first
		}
		if let fileSize = resource?.value(forKey: "fileSize") as? Int {
			completion(fileSize)
		}else {
			PHImageManager.default().requestImageData(for: phAsset, options: nil) { (data, uti, orientation, info) in
				var fileSize = -1
				if let data = data {
					let bcf = ByteCountFormatter()
					bcf.countStyle = .file
					fileSize = data.count
				}
				DispatchQueue.main.async {
					completion(fileSize)
				}
			}
		}
	}
	
	public func videoSize(options: PHVideoRequestOptions? = nil, completion: @escaping ((Int)->Void)) {
		guard let phAsset = self.phAsset, self.type == .video else {  completion(-1); return }
		let resource = PHAssetResource.assetResources(for: phAsset).filter { $0.type == .video }.first
		if let fileSize = resource?.value(forKey: "fileSize") as? Int {
			completion(fileSize)
		}else {
			PHImageManager.default().requestAVAsset(forVideo: phAsset, options: options) { (avasset, audioMix, info) in
				func fileSize(_ url: URL?) -> Int? {
					do {
						guard let fileSize = try url?.resourceValues(forKeys: [.fileSizeKey]).fileSize else { return nil }
						return fileSize
					}catch { return nil }
				}
				var url: URL? = nil
				if let urlAsset = avasset as? AVURLAsset {
					url = urlAsset.url
				}else if let sandboxKeys = info?["PHImageFileSandboxExtensionTokenKey"] as? String, let path = sandboxKeys.components(separatedBy: ";").last {
					url = URL(fileURLWithPath: path)
				}
				let size = fileSize(url) ?? -1
				DispatchQueue.main.async {
					completion(size)
				}
			}
		}
	}
	
	func MIMEType(_ url: URL?) -> String? {
		guard let ext = url?.pathExtension else { return nil }
		if !ext.isEmpty {
			let UTIRef = UTTypeCreatePreferredIdentifierForTag("public.filename-extension" as CFString, ext as CFString, nil)
			let UTI = UTIRef?.takeUnretainedValue()
			UTIRef?.release()
			if let UTI = UTI {
				guard let MIMETypeRef = UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType) else { return nil }
				let MIMEType = MIMETypeRef.takeUnretainedValue()
				MIMETypeRef.release()
				return MIMEType as String
			}
		}
		return nil
	}
	
	private func tempCopyLivePhotos(phAsset: PHAsset,
									livePhotoRequestOptions: PHLivePhotoRequestOptions? = nil,
									localURL: URL,
									completionBlock:@escaping (() -> Void)) -> PHImageRequestID? {
		var requestOptions = PHLivePhotoRequestOptions()
		if let options = livePhotoRequestOptions {
			requestOptions = options
		}else {
			requestOptions.isNetworkAccessAllowed = true
		}
		return PHImageManager.default().requestLivePhoto(for: phAsset,
														 targetSize: UIScreen.main.bounds.size,
														 contentMode: .default,
														 options: requestOptions)
		{ (livePhotos, infoDict) in
			if let livePhotos = livePhotos {
				let assetResources = PHAssetResource.assetResources(for: livePhotos)
				assetResources.forEach { (resource) in
					if resource.type == .pairedVideo {
						PHAssetResourceManager.default().writeData(for: resource, toFile: localURL, options: nil) { (error) in
							DispatchQueue.main.async {
								completionBlock()
							}
						}
					}
				}
			}
		}
	}
	
	@discardableResult
	//convertLivePhotosToJPG
	// false : If you want mov file at live photos
	// true  : If you want png file at live photos ( HEIC )
	public func tempCopyMediaFile(videoRequestOptions: PHVideoRequestOptions? = nil,
								  imageRequestOptions: PHImageRequestOptions? = nil,
								  livePhotoRequestOptions: PHLivePhotoRequestOptions? = nil,
								  exportPreset: String = AVAssetExportPresetHighestQuality,
								  convertLivePhotosToJPG: Bool = false,
								  progressBlock:((Double) -> Void)? = nil,
								  completionBlock:@escaping ((URL,String) -> Void)) -> PHImageRequestID? {
		guard let phAsset = self.phAsset else { return nil }
		var type: PHAssetResourceType? = nil
		if phAsset.mediaSubtypes.contains(.photoLive) == true, convertLivePhotosToJPG == false {
			type = .pairedVideo
		}else {
			type = phAsset.mediaType == .video ? .video : .photo
		}
		guard let resource = (PHAssetResource.assetResources(for: phAsset).filter{ $0.type == type }).first else { return nil }
		let fileName = resource.originalFilename
		var writeURL: URL? = nil
		if #available(iOS 10.0, *) {
			writeURL = FileManager.default.temporaryDirectory.appendingPathComponent("\(fileName)")
		} else {
			writeURL = URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true).appendingPathComponent("\(fileName)")
		}
		guard var localURL = writeURL,var mimetype = MIMEType(writeURL) else { return nil }
		if type == .pairedVideo {
			return tempCopyLivePhotos(phAsset: phAsset,
									  livePhotoRequestOptions: livePhotoRequestOptions,
									  localURL: localURL,
									  completionBlock: { completionBlock(localURL, mimetype) })
		}
		switch phAsset.mediaType {
		case .video:
			var requestOptions = PHVideoRequestOptions()
			if let options = videoRequestOptions {
				requestOptions = options
			}else {
				requestOptions.isNetworkAccessAllowed = true
			}
			//iCloud download progress
			requestOptions.progressHandler = { (progress, error, stop, info) in
				DispatchQueue.main.async {
					progressBlock?(progress)
				}
			}
			return PHImageManager.default().requestExportSession(forVideo: phAsset,
																 options: requestOptions,
																 exportPreset: exportPreset)
			{ (session, infoDict) in
				session?.outputURL = localURL
				session?.outputFileType = AVFileType.mov
				session?.exportAsynchronously(completionHandler: {
					DispatchQueue.main.async {
						completionBlock(localURL, mimetype)
					}
				})
			}
		case .image:
			var requestOptions = PHImageRequestOptions()
			if let options = imageRequestOptions {
				requestOptions = options
			}else {
				requestOptions.isNetworkAccessAllowed = true
			}
			//iCloud download progress
			requestOptions.progressHandler = { (progress, error, stop, info) in
				DispatchQueue.main.async {
					progressBlock?(progress)
				}
			}
			return PHImageManager.default().requestImageData(for: phAsset,
															 options: requestOptions)
			{ (data, uti, orientation, info) in
				do {
					var data = data
					let needConvertLivePhotoToJPG = phAsset.mediaSubtypes.contains(.photoLive) == true && convertLivePhotosToJPG == true
					if needConvertLivePhotoToJPG {
						let name = localURL.deletingPathExtension().lastPathComponent
						localURL.deleteLastPathComponent()
						localURL.appendPathComponent("\(name).jpg")
						mimetype = "image/jpeg"
					}
					if needConvertLivePhotoToJPG, let imgData = data, let rawImage = UIImage(data: imgData)?.upOrientationImage() {
						data = rawImage.jpegData(compressionQuality: 1)
					}
					try data?.write(to: localURL)
					DispatchQueue.main.async {
						completionBlock(localURL, mimetype)
					}
				}catch { }
			}
		default:
			return nil
		}
	}
	
	private func videoFilename(phAsset: PHAsset) -> URL? {
		guard let resource = (PHAssetResource.assetResources(for: phAsset).filter{ $0.type == .video }).first else {
			return nil
		}
		var writeURL: URL?
		let fileName = resource.originalFilename
		if #available(iOS 10.0, *) {
			writeURL = FileManager.default.temporaryDirectory.appendingPathComponent("\(fileName)")
		} else {
			writeURL = URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true).appendingPathComponent("\(fileName)")
		}
		return writeURL
	}
	
	//Apparently, This is not the only way to export video.
	//There is many way that export a video.
	//This method was one of them.
	public func exportVideoFile(options: PHVideoRequestOptions? = nil,
								outputURL: URL? = nil,
								outputFileType: AVFileType = .mov,
								progressBlock:((Double) -> Void)? = nil,
								completionBlock:@escaping ((URL,String,Data?) -> Void)) {
		guard
			let phAsset = self.phAsset,
			phAsset.mediaType == .video,
			let writeURL = outputURL ?? videoFilename(phAsset: phAsset),
			let mimetype = MIMEType(writeURL)
			else {
				return
		}
		var requestOptions = PHVideoRequestOptions()
		if let options = options {
			requestOptions = options
		}else {
			requestOptions.isNetworkAccessAllowed = true
		}
//		self.mediaMimeType = mimetype
		requestOptions.progressHandler = { (progress, error, stop, info) in
			DispatchQueue.main.async {
				progressBlock?(progress)
			}
		}
		PHImageManager.default().requestAVAsset(forVideo: phAsset, options: requestOptions) { (avasset, avaudioMix, infoDict) in
			guard let avasset = avasset else {
				return
			}
			let exportSession = AVAssetExportSession.init(asset: avasset, presetName: AVAssetExportPresetHighestQuality)
			exportSession?.outputURL = writeURL
			exportSession?.outputFileType = outputFileType
			let videoData = try? Data(contentsOf: writeURL)
			exportSession?.exportAsynchronously(completionHandler: {
				completionBlock(writeURL, mimetype, videoData ?? nil)
			})
		}
	}
	
	init(asset: PHAsset?) {
		super.init()
		self.phAsset = asset
	}
	
//	required public init(from decoder: Decoder) throws {
//		fatalError("init(from:) has not been implemented")
//	}
	
//	required init(from decoder: Decoder) throws {
//		fatalError("init(from:) has not been implemented")
//	}
	
	
	public static func asset(with localIdentifier: String) -> CMAppMedia? {
		let fetchResult = PHAsset.fetchAssets(withLocalIdentifiers: [localIdentifier], options: nil)
		return CMAppMedia(asset: fetchResult.firstObject)
	}
	public static func ==(lhs: CMAppMedia, rhs: CMAppMedia) -> Bool {
		guard let lphAsset = lhs.phAsset, let rphAsset = rhs.phAsset else { return false }
		return lphAsset.localIdentifier == rphAsset.localIdentifier
	}

}

extension String {
	public var lastPathComponent: String {
		get {
			return (self as NSString).lastPathComponent
		}
	}
	public var pathExtension: String {
		get {
			
			return (self as NSString).pathExtension
		}
	}
}
