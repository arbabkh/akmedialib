//
//  AKMediaImageView.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 27/06/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import UIKit
import SDWebImage

open class AKMediaImageView: UIView {
	
	@IBOutlet open weak var containerViewImgPic: UIView!
	@IBOutlet open weak var imgPic: UIImageView!
	@IBOutlet open weak var btnRemove: UIButton!
	@IBOutlet open weak var btnRemoveView: AKCustomView!
	@IBOutlet open weak var btnEdit: UIButton!
	@IBOutlet open weak var btnEditView: AKCustomView!

	@IBOutlet open weak var placeHolderPic: UIButton!
	@IBOutlet weak var widthContraintBtnLogo: NSLayoutConstraint!
	@IBOutlet weak var heightContraintBtnLogo: NSLayoutConstraint!

	var mMedia : CMAppMedia?
	open var placeHolderImage: UIImage?
	open var widthIcon:CGFloat = 100
	open var heightIcon:CGFloat = 100

	open var themeColor: UIColor =  UIColor(red: 254/255, green: 76/255, blue: 76/255, alpha: 1.0)
	
	open var isRoundedCorners = false
	open var viewContentMode = UIView.ContentMode.scaleAspectFill
	open var isEditingMode = false
	open var hideEditBtn = true //temp

	open var didActionCallBack: ((Any, String)->Void)?

	open override func awakeFromNib() {
		super.awakeFromNib()
		let removeIcon = AKBundle.podBundleImage(named: "crossIconWhite")
		btnRemove.setImage(removeIcon, for: .normal)
		btnRemove.tintColor = UIColor.white
	}
	
	open func updateCellWithData(inMedia : CMAppMedia?) {
		mMedia = inMedia
		downLoadImage()
		setNeedsLayout()
	}
	
	@IBAction func clickedOnBtnRemove(_ sender: Any) {
		didActionCallBack?(mMedia!, "deleteMedia")
	}
	@IBAction func clickedOnBtnEdit(_ sender: Any) {
		didActionCallBack?(mMedia!, "editMedia")
	}
	
	func downLoadImage() {
		placeHolderPic.setImage(placeHolderImage, for: .normal)
		placeHolderPic.tintColor = themeColor

		imgPic.contentMode = viewContentMode
		placeHolderPic.isHidden = true
		if mMedia?.appMediaType == enAppMediaType.eMediaTypeDefaultLocalImage {
			imgPic.image = mMedia?.croppedImage
		} else {
			placeHolderPic.isHidden = false
			imgPic.backgroundColor = themeColor
			imgPic.alpha = 0.1
			var mediaUrlStr: String?
			if mMedia?.appMediaType == enAppMediaType.eMediaTypeVideo {
				mediaUrlStr = mMedia?.strThumbnFullPath
			} else {
				mediaUrlStr = mMedia?.strMediaFullPath
			}
			guard mediaUrlStr!.count != 0 else {
				return
			}
			imgPic.sd_setImage(with: URL(string: mediaUrlStr!), placeholderImage: nil, options: .highPriority) { [weak self](image, error, inCacheType, inImageUrl) in
				DispatchQueue.main.async {
					if image != nil {
						self?.mMedia!.width = image!.size.width
						self?.mMedia!.height = image!.size.height
						self?.placeHolderPic.isHidden = true
						self?.imgPic.alpha = 1
					}
				}
			}
		}
	}
	
	open override func layoutSubviews() {
		super.layoutSubviews()
		if isRoundedCorners {
			containerViewImgPic.layer.cornerRadius = 8
			containerViewImgPic.layer.borderColor = UIColor.lightGray.cgColor
			containerViewImgPic.layer.borderWidth = 0.5
		} else {
			containerViewImgPic.layer.cornerRadius = 0
			containerViewImgPic.layer.borderColor = UIColor.clear.cgColor
			containerViewImgPic.layer.borderWidth = 0.0
		}
		btnRemoveView.isHidden = !isEditingMode
		btnEditView.isHidden = hideEditBtn

		widthContraintBtnLogo.constant =  widthIcon
		heightContraintBtnLogo.constant = heightIcon
	}
}
