//
//  AKMediaImageCollectionCell.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 27/06/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import UIKit

open class AKMediaImageCollectionCell: UICollectionViewCell {
	
	@IBOutlet weak var containerImageView: UIView!
	
	open var indexPath: IndexPath!

	open var isRoundedCorners = false
	open var viewContentMode = UIView.ContentMode.scaleAspectFill
	open var isEditingMode = false
	open var hideEditBtn = true //temp

	open var didActionCallBack: ((Any, String)->Void)?

	open var mMedia : CMAppMedia?

	open var placeHolderImage: UIImage?
	open var widthIcon:CGFloat = 100
	open var heightIcon:CGFloat = 100

	open lazy var mediaImageView : AKMediaImageView = {
		var mediaImageView = AKBundle.bundle().loadNibNamed("AKMediaImageView", owner: nil, options: nil)?.first as? AKMediaImageView
		mediaImageView?.frame = CGRect(x: 0, y: 0, width: containerImageView.frame.size.width, height: containerImageView.frame.size.height)
		mediaImageView?.viewContentMode = viewContentMode
		return mediaImageView!
	}()

	open override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		containerImageView.addSubview(mediaImageView)
    }

	open func updateCellWithData(inMedia : CMAppMedia?) {
		mMedia = inMedia
		mediaImageView.isRoundedCorners = isRoundedCorners
		mediaImageView.widthIcon = widthIcon
		mediaImageView.placeHolderImage = placeHolderImage
		mediaImageView.heightIcon = heightIcon
		mediaImageView.isEditingMode = isEditingMode
		mediaImageView.hideEditBtn = hideEditBtn
		mediaImageView.viewContentMode = viewContentMode
		mediaImageView.updateCellWithData(inMedia: mMedia)
		mediaImageView.didActionCallBack = {[weak self] (obj, actionId) in
			self?.didActionCallBack?(obj, actionId)
		}
		setNeedsLayout()
	}
		
	open override func layoutSubviews() {
		super.layoutSubviews()
		
		mediaImageView.frame = CGRect(x: 0,
									  y: 0,
									  width: containerImageView.frame.size.width,
									  height: containerImageView.frame.size.height)
	}
	
}
