//
//  AKBundle.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 18/12/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import UIKit

open class AKBundle {
	open class func podBundleImage(named: String) -> UIImage? {
		let podBundle = Bundle(for: AKBundle.self)
		if let url = podBundle.url(forResource: "AKMedia", withExtension: "bundle") {
			let bundle = Bundle(url: url)
			return UIImage(named: named, in: bundle, compatibleWith: nil)
		}
		return nil
	}
	
	open class func bundle() -> Bundle {
		let podBundle = Bundle(for: AKBundle.self)
		if let url = podBundle.url(forResource: "AKMediaLib", withExtension: "bundle") {
			let bundle = Bundle(url: url)
			return bundle ?? podBundle
		}
		return podBundle
	}
}
