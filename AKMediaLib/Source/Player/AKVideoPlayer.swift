//
//  AKVideoPlayer.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 30/10/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import Foundation
import AVFoundation

open class AKVideoPlayer: AVPlayerLayer {
	
	open var mediaId: String = ""
	open var isMuted = true
	open var isAutoPlay = true
	open var observerObject: NSObjectProtocol?

	
	override init() {
		super.init()
		isMuted = AKVideoMgr.shared.isMuted
		
	}
	
	required public init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	open func addObserver() {
		
		NotificationCenter.default.addObserver(self, selector: #selector(listenerForVideoAction), name: NSNotification.Name(rawValue: "kStopAllVideos"), object: nil)
		player?.actionAtItemEnd = AVPlayer.ActionAtItemEnd.none
		
		NotificationCenter.default.addObserver(self, selector: #selector(listenerForVideoDidFinish), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
		NotificationCenter.default.addObserver(self, selector: #selector(listenerForVideoAction), name: NSNotification.Name(rawValue: "kMuteUnMuteAllVideos"), object: nil)

	}
	
	@objc func listenerForVideoAction(inNotification:Notification) {
		if inNotification.name.rawValue == "kStopAllVideos" {
			player?.pause()
		} else if inNotification.name.rawValue == "kMuteUnMuteAllVideos" {
			let muted = inNotification.object
			guard muted != nil else { return }
			player?.isMuted = muted as! Bool
			isMuted = muted as! Bool
		}
	}
	@objc func listenerForVideoDidFinish(inNotification:Notification) {
		let playerItem = inNotification.object as? AVPlayerItem
		playerItem?.seek(to: CMTime.zero, completionHandler: nil)
		if !isAutoPlay {
			player?.pause()
		}
	}
	open func removeObserverObject() {
		if let observer = self.observerObject {
			player?.removeTimeObserver(observer)
		}
		observerObject = nil
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
		if let observer = self.observerObject {
			player?.removeTimeObserver(observer)
		}
	}
	
}

