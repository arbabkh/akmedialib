//
//  AKVideoMgr.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 30/10/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
open class AKVideoMgr: NSObject {
	
	public static var shared = AKVideoMgr()
	private var observerContext: NSObjectProtocol?
	
	var oldMusicCategory = ""

	open var isMuted = true
	
	lazy var loadedMediaList = [String:AKVideoPlayer]()
	
	private override init() {
		super.init()
		addObservers()
		oldMusicCategory = AVAudioSession.sharedInstance().category.rawValue
		do {
			try AVAudioSession.sharedInstance().setActive(true)
		} catch {
			print(error)
		}
	}
	
	deinit {
		self.removeAllObservers()
	}
	func addObservers() {
		NotificationCenter.default.addObserver(self, selector: #selector(listenerForEnterInBackground), name: UIApplication.willResignActiveNotification, object: nil)
		AVAudioSession.sharedInstance().addObserver(self, forKeyPath: "outputVolume", options: .new, context: &observerContext)
	}
	func removeAllObservers() {
		self.removeObserver(self, forKeyPath: "outputVolume", context: &observerContext)
		NotificationCenter.default.removeObserver(self)
	}
	
	
	@objc func listenerForEnterInBackground(inNotification:Notification) {
		if inNotification.name == UIApplication.willResignActiveNotification {
			pauseAllVideos()
		}
	}
	
	func setAVAudiosessionCategory(inCategory: String) {
		do {
			try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category(rawValue: inCategory))
		} catch {
			print(error)
		}
	}
	
	open override func observeValue(forKeyPath keyPath: String?,
								   of object: Any?,
								   change: [NSKeyValueChangeKey : Any]?,
								   context: UnsafeMutableRawPointer?) {
		if &observerContext == context {
			if keyPath == "outputVolume" {
				if isMuted {
					muteUnMuteAllVideos(inIsMute: false)
				}
			}
		}
	}
	
	open func addVideoInListIfNeeded(idMedia: CMAppMedia) {
		if idMedia.appMediaType == enAppMediaType.eMediaTypeVideo || idMedia.appMediaType == enAppMediaType.eMediaTypeDefaultLocalVideo {
			var aPlayer = loadedMediaList[idMedia.mediaId ?? "1"]
			if aPlayer == nil {
				let mediaFullPathUrl:URL = URL(string: idMedia.strMediaFullPath)!
				let item = AVPlayerItem.init(url: idMedia.appMediaType == enAppMediaType.eMediaTypeDefaultLocalVideo ? idMedia.mediaLocalUrl! : mediaFullPathUrl)
				aPlayer = AKVideoPlayer()
				aPlayer?.player = AVPlayer.init(playerItem: item)
				aPlayer?.isAutoPlay = idMedia.isAutoPlay
				aPlayer?.addObserver()
				let mediaId = idMedia.mediaId

				if !(mediaId ?? "1").isEmpty {
					aPlayer?.mediaId = mediaId!
					loadedMediaList[mediaId!] = aPlayer
				}
				aPlayer?.player?.isMuted = isMuted
			}
			aPlayer!.frame = CGRect(x: 0, y: 0, width: idMedia.mediaSize.width, height: idMedia.mediaSize.height)
			aPlayer!.videoGravity = idMedia.videoGraviity
		}
	}
	
	open func getCurrentPlayer(idMedia: CMAppMedia) -> AKVideoPlayer {
		return loadedMediaList[idMedia.mediaId ?? "1"]!
	}
	
	open func getCurrentAVPlayer(idMedia: CMAppMedia) -> AVPlayer {
		var aPlayer = loadedMediaList[idMedia.mediaId ?? "1"]
		if aPlayer == nil {
			let mediaFullPathUrl:URL = URL(string: idMedia.strMediaFullPath)!
			let item = AVPlayerItem.init(url: idMedia.appMediaType == enAppMediaType.eMediaTypeDefaultLocalVideo ? idMedia.mediaLocalUrl! : mediaFullPathUrl)
			aPlayer = AKVideoPlayer.init()
			aPlayer!.player = AVPlayer.init(playerItem: item)
		}
		return aPlayer!.player!
	}
	
	open func pauseVideo(idMedia: CMAppMedia) {
		let aPlayer = loadedMediaList[idMedia.mediaId ?? "1"]
		if aPlayer != nil {
			if aPlayer!.player!.timeControlStatus == .playing {
				aPlayer!.player!.pause()
				setAVAudiosessionCategory(inCategory: oldMusicCategory)
			}
		}
	}
	
	open func playVideo(idMedia: CMAppMedia) {
		let aPlayer = loadedMediaList[idMedia.mediaId ?? "1"]
		if aPlayer != nil {
			setAVAudiosessionCategory(inCategory: AVAudioSession.Category.playback.rawValue)
			aPlayer!.player!.play()
		}
	}
	
	open func pauseAllVideos() {
		NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kStopAllVideos"), object: nil)
	}
	
	open func muteUnMuteAllVideos(inIsMute: Bool) {
		isMuted = inIsMute
		NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kMuteUnMuteAllVideos"), object: inIsMute)
		NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kUpdateUIMuteUnMuteAllVideos"), object: inIsMute)

	}
	
	//This is method to play video in
	open func playVideoInNativeWindow(inUrlStri: String) {
		
	}
	
}

