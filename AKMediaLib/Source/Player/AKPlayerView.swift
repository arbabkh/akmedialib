//
//  AKPlayerView.swift
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 30/10/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

import UIKit
import AVFoundation

open class AKPlayerView: UIView {

	private var observerContext = 0

	open var mCurrentPlayer: AKVideoPlayer?
	open var mCurrentMedia: CMAppMedia?
	
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet weak var bottomViewContainer: UIView!
	@IBOutlet weak var timeAndSeekbarContainerView: UIView!
	@IBOutlet weak var lblWatchedTime: UILabel!
	@IBOutlet weak var seekbarSlider: UISlider!
	@IBOutlet weak var lblTotalTime: UILabel!


	var isHideControls = true
	var isPlaying = true
	var timeInterval = CMTime(seconds: 1.0, preferredTimescale: CMTimeScale(1.0))
	
	open var didClickedOnMuteToggleButton: ((AKPlayerView,Bool)->Void)?
	open var didChangeSeekSlider: ((AKPlayerView, Any)->Void)?

	
	open override func awakeFromNib() {
		super.awakeFromNib()
		self.hideControls(inHide: isHideControls)
		self.addObservers()
	}
	
	deinit {
		removeObserverObject()
	}
	
	func addObservers() {
		NotificationCenter.default.addObserver(self, selector: #selector(listenerForVideoAction), name: NSNotification.Name(rawValue: "kUpdateUIMuteUnMuteAllVideos"), object: nil)
	}
	
	func removeObserverObject() {
		
		mCurrentPlayer?.player?.removeObserver(self, forKeyPath: "playbackBufferEmpty", context: &observerContext)
		mCurrentPlayer?.player?.removeObserver(self, forKeyPath: "playbackLikelyToKeepUp", context: &observerContext)
		mCurrentPlayer?.player?.removeObserver(self, forKeyPath: "playbackBufferFull", context: &observerContext)
		NotificationCenter.default.removeObserver(self)

	}
	
	@objc func listenerForVideoAction(inNotification:Notification) {
		if inNotification.name.rawValue == "kUpdateUIMuteUnMuteAllVideos" {
			guard let isMuted = inNotification.object as? Bool else { return }
			didClickedOnMuteToggleButton?(self, isMuted)
		}
	}
	
	func updateUIContentWithData(inMedia: CMAppMedia) {
		mCurrentMedia = inMedia
		let aPlayer = AKVideoMgr.shared.getCurrentPlayer(idMedia: inMedia)
		mCurrentPlayer?.removeObserverObject()
		aPlayer.observerObject = nil
		mCurrentPlayer = aPlayer
		if isHideControls {
			self.layer.insertSublayer(aPlayer, at: 0)
		}
		addObserverTimeForPlayer(inPlayer: mCurrentPlayer?.player ?? AVPlayer())
		
	}
	@IBAction func valuesChangedOnSeekSlider(_ sender: Any) {
		didChangeSeekSlider?(self, seekbarSlider.value)
	}
	
	func addObserverTimeForPlayer(inPlayer: AVPlayer) {
		if mCurrentPlayer?.observerObject == nil {
			mCurrentPlayer?.observerObject = inPlayer.addPeriodicTimeObserver(forInterval: timeInterval, queue: DispatchQueue.main, using: {[weak self] (inElapseTime) in
				self?.updateSeekData(inElapsedTime: inElapseTime)
				self?.startAnimatedActivity(inStart: false)
			}) as? NSObjectProtocol
			addObserverBufferedTimeForPlayer(inPlayer: inPlayer)
		}
	}
	
	func addObserverBufferedTimeForPlayer(inPlayer: AVPlayer) {
		inPlayer.addObserver(self, forKeyPath: "playbackBufferEmpty", options: NSKeyValueObservingOptions.new, context:&observerContext)
		inPlayer.addObserver(self, forKeyPath: "playbackLikelyToKeepUp", options: NSKeyValueObservingOptions.new, context:&observerContext)
		inPlayer.addObserver(self, forKeyPath: "playbackBufferFull", options: NSKeyValueObservingOptions.new, context:&observerContext)

	}
	
	func updateSeekData(inElapsedTime: CMTime) {
		let duration = CMTimeGetSeconds(mCurrentPlayer?.player?.currentItem?.duration ?? CMTime())
		if duration >= 0 {
			let elapsedTime = CMTimeGetSeconds(inElapsedTime);
			let progress  = elapsedTime/duration;
			seekbarSlider.setValue(Float(progress), animated: true)
			lblWatchedTime.text = "\((lround(elapsedTime)/60)%60)" + ":" + "\(lround(elapsedTime)%60)"
			lblTotalTime.text = "\((lround(duration)/60)%60)" + ":" + "\(lround(duration)%60)"
			didChangeSeekSlider?(self, elapsedTime)
		}
	}
	func hideControls(inHide: Bool) {
		bottomViewContainer.isHidden = inHide
	}
	func startAnimatedActivity(inStart: Bool) {
		if inStart {
			activityIndicator.startAnimating()
		} else {
			activityIndicator.stopAnimating()
		}
	}
	
	open override func observeValue(forKeyPath keyPath: String?,
								   of object: Any?,
								   change: [NSKeyValueChangeKey : Any]?,
								   context: UnsafeMutableRawPointer?) {
		if &observerContext == context {
			if keyPath == "playbackBufferEmpty" {
				startAnimatedActivity(inStart: true)
			} else if keyPath == "playbackLikelyToKeepUp" {
				startAnimatedActivity(inStart: false)
			} else if keyPath == "playbackBufferFull" {
				startAnimatedActivity(inStart: false)
			}
		}
	}

}
