//
//  AKMediaLib.h
//  AKMediaLib
//
//  Created by ArbabAhmedKhan on 28/06/20.
//  Copyright © 2020 LogiRay. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for AKMediaLib.
FOUNDATION_EXPORT double AKMediaLibVersionNumber;

//! Project version string for AKMediaLib.
FOUNDATION_EXPORT const unsigned char AKMediaLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <AKMediaLib/PublicHeader.h>


